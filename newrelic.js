'use strict'

/**
 *  * New Relic agent configuration.
 *   *
 *    * See lib/config.defaults.js in the agent distribution for a more complete
 *     * description of configuration variables and their potential values.
 *      */
exports.config = {
  /**
 *    * Array of application names.
 *       */
  app_name: ["sparkpad-dev-wm"],
  /**
 *    * Your New Relic license key.
 *       */
  license_key: "44d69e5dd03a38d50a94161137ad0e44f33761da",
  logging: {
    /**
 *      * Level at which to log. 'trace' is most useful to New Relic when diagnosing
 *           * issues with the agent, 'info' and higher will impose the least overhead on
 *                * production applications.
 *                     */
    level: 'info'
  }
}

